import javafx.embed.swing.SwingFXUtils
import javafx.fxml.FXML
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.image.PixelReader
import javafx.scene.paint.Color
import java.io.File
import javax.imageio.ImageIO

class OcrController {

    @FXML
    lateinit var canvas: Canvas

    @FXML
    lateinit var resultLabel: Label

    @FXML
    lateinit var ocrButton: Button

    @FXML
    lateinit var resetButton: Button

    private val brushSize = 3.0
    private val ocr = OcrProcessor()

    fun initialize() {
        val g = canvas.graphicsContext2D

        canvas.setOnMouseDragged {
            val x = it.x - brushSize / 2
            val y = it.y - brushSize / 2

            g.fill = Color.BLACK
            g.fillRect(x, y, brushSize, brushSize)
        }
    }

    fun clearCanvas() {
        ImageIO.write(
                SwingFXUtils.fromFXImage(canvas.snapshot(null, null), null),
                "png", File("pint666.png"))

        val g = canvas.graphicsContext2D
        g.clearRect(0.0, 0.0, canvas.width, canvas.height)
        resultLabel.text = "?"
    }

    fun recognize() {
        resultLabel.text = when(
            ocr.run(canvas.snapshot(null, null))) {
            0 -> "квадрат"
            1 -> "круг"
            2 -> "треугольник"
            else -> "не знаю..."
        }
    }
}