import javafx.embed.swing.SwingFXUtils
import javafx.scene.canvas.Canvas
import javafx.scene.image.Image
import javafx.scene.image.WritableImage
import javafx.scene.paint.Color
import java.io.File
import java.util.*
import java.util.ArrayList
import javax.imageio.ImageIO


class OcrProcessor {

    private var width = 0.0
    private var height = 0.0
    private var trainSet = ArrayList<ArrayList<WritableImage>>()

    private val trainSize = 10
    private var potentialMatrix = ArrayList<Double>()

    // 0 - square
    // 1 - circle
    // 2 - triangle
    private fun generateTrainSet() {
        trainSet.clear()
        trainSet.add(ArrayList()) // 0
        trainSet.add(ArrayList()) // 1
        trainSet.add(ArrayList()) // 2

        val canvas = Canvas()
        canvas.width = width
        canvas.height = height
        canvas.graphicsContext2D.stroke = Color.BLACK
        canvas.graphicsContext2D.fill = Color.BLACK
        for (i in 0 until trainSize) {
            val offset = 10 + Random().nextDouble() * 10
            for (shape in 0 until 3) {
                when(shape) {
                    0 -> {  // Square
                        canvas.graphicsContext2D.strokeRect(offset, offset, width - offset * 2, height - offset * 2)
                    }
                    1 -> {  // Circle
                        val w = width - offset * 2
                        val h = height - offset * 2
                        canvas.graphicsContext2D.strokeOval((width - w) / 2, (height - h) / 2, w, h)
                    }
                    2 -> {  // Triangle
                        canvas.graphicsContext2D.lineWidth = 2.0
                        canvas.graphicsContext2D.beginPath()
                        canvas.graphicsContext2D.moveTo(width / 2, offset)
                        canvas.graphicsContext2D.lineTo(offset, width - offset)
                        canvas.graphicsContext2D.lineTo(width - offset, width - offset)
                        canvas.graphicsContext2D.closePath()
                        canvas.graphicsContext2D.stroke()
                    }
                }
                // Save images to files
                ImageIO.write(
                        SwingFXUtils.fromFXImage(canvas.snapshot(null, null), null),
                        "png", File("pint${shape}_$i.png"))
                // Store in object
                trainSet[shape].add(canvas.snapshot(null, null))
                canvas.graphicsContext2D.clearRect(0.0, 0.0, canvas.width, canvas.height)
            }
        }
    }

    private fun hammingLength(a: WritableImage, b: WritableImage): Int {
        if (a.width != b.width || a.height != b.height) {
            return -1
        }

        var r = 0
        for (i in 0 until a.width.toInt()) {
            for (j in 0 until a.height.toInt()) {
                if (a.pixelReader.getColor(i, j) != b.pixelReader.getColor(i, j)) {
                    r++
                }
            }
        }

        return  r
    }

    private fun calcPotentials(image: WritableImage) {
        potentialMatrix.clear()
        for (shape in 0 until 3) {
            var potential = 0.0
            for (i in 0 until trainSize) {
                val r = hammingLength(image, trainSet[shape][i])
                potential += 1000000.0 / (1 + r.times(r))
            }
            potentialMatrix.add(potential)
        }
        // Debug
        println(potentialMatrix)
    }

    fun run(image: WritableImage): Int {
        width = image.width
        height = image.height

        if (trainSet.isEmpty()) {
            generateTrainSet()
        }

        calcPotentials(image)
        return potentialMatrix.indices.maxBy { potentialMatrix[it] } ?: -1
    }
}